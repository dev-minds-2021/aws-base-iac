################################################################################################
# PROVIDER:A Plugin that helps you speak to multiple cloud providers, in our case aws is defined
#
#################################################################################################

provider "aws" {
  version = "~>2.0"
  profile = "admin_preprod"
  region  = var.region
}


resource "aws_launch_configuration" "webapp_lc" {
  lifecycle {
    create_before_destroy = true
  }

  name_prefix   = "${terraform.workspace}-dm-lc-"
  image_id      = data.aws_ami.ubuntu_ami.id
  instance_type = var.asg_instance_size
  key_name      = "preprod"

  security_groups = [
    aws_security_group.webapp_http_inbound_sg.id,
    aws_security_group.webapp_ssh_inbound_sg.id,
    aws_security_group.webapp_outbound_sg.id,
  ]

  user_data                   = data.template_file.userd.rendered
  associate_public_ip_address = true
  iam_instance_profile        = aws_iam_instance_profile.asg.name
}


resource "aws_autoscaling_group" "webapp_asg" {
  lifecycle {
    create_before_destroy = false
  }

  #   vpc_zone_identifier   = data.terraform_remote_state.networking.outputs.net_vpc_subnets
  vpc_zone_identifier = ["subnet-0c03dfc893809d2a9"]
  name                = "${terraform.workspace}-dm-asg"
  max_size            = var.asg_max_size
  min_size            = var.asg_min_size
  desired_capacity    = var.running_nodes
  # wait_for_elb_capacity = var.asg_min_size
  force_delete         = true
  launch_configuration = aws_launch_configuration.webapp_lc.id
  load_balancers       = [aws_elb.webapp_elb.name]

  dynamic "tag" {
    for_each = local.common_tags
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
}

resource "aws_elb" "webapp_elb" {
  name = "${terraform.workspace}-dm-elb"
  #   subnets = data.terraform_remote_state.networking.outputs.public_subnets
  subnets = ["subnet-0c03dfc893809d2a9"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    # target              = "HTTP:80/"
    target   = "TCP:22"
    interval = 10
  }

  security_groups = [aws_security_group.webapp_http_inbound_sg.id]
  tags            = local.common_tags
}



resource "aws_autoscaling_policy" "scale_up" {
  name                   = "${terraform.workspace}-dm-asg-scale-up-rules"
  scaling_adjustment     = 2
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.webapp_asg.name
}

resource "aws_cloudwatch_metric_alarm" "scale_up_alarm" {
  alarm_name                = "${terraform.workspace}-dm-asg-cw-su-alarm"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = "2"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = "120"
  statistic                 = "Average"
  threshold                 = "80"
  insufficient_data_actions = []

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.webapp_asg.name
  }

  alarm_description = "EC2 CPU Utilization"
  alarm_actions     = [aws_autoscaling_policy.scale_up.arn]
}

resource "aws_autoscaling_policy" "scale_down" {
  name                   = "${terraform.workspace}-dm-asg-scaledown-policy"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 600
  autoscaling_group_name = aws_autoscaling_group.webapp_asg.name
}

resource "aws_cloudwatch_metric_alarm" "scale_down_alarm" {
  alarm_name                = "${terraform.workspace}-dm-asg-cw-sd-alarm"
  comparison_operator       = "LessThanThreshold"
  evaluation_periods        = "5"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = "120"
  statistic                 = "Average"
  threshold                 = "30"
  insufficient_data_actions = []

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.webapp_asg.name
  }

  alarm_description = "EC2 CPU Utilization"
  alarm_actions     = [aws_autoscaling_policy.scale_down.arn]
}


# resource "aws_db_subnet_group" "db_subnet_group" {
#   name = "${terraform.workspace}-ddt-rds-subnet-group"
#   #   subnet_ids = data.terraform_remote_state.networking.outputs.net_vpc_subnets
#   subnet_ids = ["subnet-001355bd9993e3bda"]
# }