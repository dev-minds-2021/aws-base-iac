locals {
  #   bucket_name = "${var.bket_name_prefix}-${local.env_name}-${random_integer.this_random_vals.result}"
  env_name = lower(terraform.workspace)
  common_tags = {
    BillingCodes = var.billing_code_tag
    Environment  = local.env_name
  }
}

variable "region" {
  default = "eu-west-1"
}

variable "billing_code_tag" {}
variable "asg_instance_size" {}
variable "asg_max_size" {}
variable "asg_min_size" {}
variable "running_nodes" {}
variable "ip_range" {}
variable "playbook_repository" {}

variable "aws_bucket_prefix" {
  type    = string
  default = "dminds"
}

locals {
  bucket_name = "${var.aws_bucket_prefix}-${random_integer.rand.result}"
}
