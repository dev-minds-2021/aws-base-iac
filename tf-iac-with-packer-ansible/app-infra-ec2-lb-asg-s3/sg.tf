##################################################################################
# RESOURCES
##################################################################################

resource "aws_security_group" "webapp_http_inbound_sg" {
  name        = "${terraform.workspace}-dm-sg-http"
  description = "Allow HTTP from Anywhere"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #   vpc_id = data.terraform_remote_state.networking.outputs.net_vpc_id
  vpc_id = "vpc-0ae501218870f2770"

  tags = {
    Name = "${terraform.workspace}-dm-sg-http"
  }
}

resource "aws_security_group" "webapp_ssh_inbound_sg" {
  name        = "${terraform.workspace}-dm-sg-ssh"
  description = "Allow SSH from certain ranges"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.ip_range]
  }

  #   vpc_id = data.terraform_remote_state.networking.outputs.net_vpc_id
  vpc_id = "vpc-0ae501218870f2770"

  tags = merge(local.common_tags, {
    Name = "${terraform.workspace}-dm-sg-ssh"
  })
}

resource "aws_security_group" "webapp_outbound_sg" {
  name        = "${terraform.workspace}-dm-sg-outbound-egress"
  description = "Allow outbound connections"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #vpc_id = data.terraform_remote_state.networking.outputs.net_vpc_id
  vpc_id = "vpc-0ae501218870f2770"

  tags = merge(local.common_tags, {
    Name = "terraform_demo_webapp_outbound"
  })
}

#resource "aws_security_group" "rds_sg" {
#  name        = "demo_rds_inbound"
#  description = "Allow inbound from web tier"
#  #vpc_id      = data.terraform_remote_state.networking.outputs.net_vpc_id
#  vpc_id = "vpc-0081ea95abfec62fa"
#
#  tags = {
#    Name = "demo_rds_inbound"
#  }
#
#  // allows traffic from the SG itself
#  ingress {
#    from_port = 0
#    to_port   = 0
#    protocol  = "-1"
#    self      = true
#  }

#  // allow traffic for TCP 3306
#  ingress {
#    from_port       = 3306
#    to_port         = 3306
#    protocol        = "tcp"
#    security_groups = [aws_security_group.webapp_http_inbound_sg.id]
#  }

#  // outbound internet access
#  egress {
#    from_port   = 0
#    to_port     = 0
#    protocol    = "-1"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
#}
