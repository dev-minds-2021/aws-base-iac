terraform {
  required_version = ">= 0.12.26"

  backend "s3" {
    bucket = "dm-vpc-states"
    key    = "fullstack/webapp.tfstate"
    region = "eu-west-1"
  }
}
