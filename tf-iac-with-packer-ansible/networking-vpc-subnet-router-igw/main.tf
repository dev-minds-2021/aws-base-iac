
################################################################################################
# TASK: We want to develop|script a network bed for our ec2 instances to run on 
# PROVIDER:A Plugin that helps you speak to multiple cloud providers, in our case aws is defined
# MUST: You always need to define this to tell tf whihch provider you are using
# We arr using the datasource to fetch all AZs in our eu-west region 
################################################################################################

provider "aws" {
  version = "~>2.0"
  profile = "admin_preprod"
  region  = var.region
}

data "aws_availability_zones" "this_ds_azs" {}
resource "random_integer" "this_random_vals" {
  min = 1000000
  max = 9999900
}
######################################################################################################
# At this point we have performed relevant rituals to start actually creating resources from services
######################################################################################################
resource "aws_vpc" "this_vpc" {
  cidr_block = var.net_addr_space[terraform.workspace]
  tags = merge(
    local.common_tags, { Name = "${local.env_name}-vpc" }
  )
}

resource "aws_internet_gateway" "this_igw" {
  vpc_id = aws_vpc.this_vpc.id

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-igw" }
  )
}

resource "aws_subnet" "this_subnet" {
  count                   = var.subnet_count[terraform.workspace]
  cidr_block              = cidrsubnet(var.net_addr_space[terraform.workspace], 8, count.index)
  vpc_id                  = aws_vpc.this_vpc.id
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.this_ds_azs.names[count.index]

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-subnet${count.index + 1}" }
  )
}

resource "aws_route_table" "this_rtb" {
  vpc_id = aws_vpc.this_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this_igw.id
  }

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-rtb" }
  )
}

resource "aws_route_table_association" "this_rtb_sb_assoc" {
  count          = var.subnet_count[terraform.workspace]
  subnet_id      = aws_subnet.this_subnet[count.index].id
  route_table_id = aws_route_table.this_rtb.id
}

resource "aws_security_group" "this_elb_sg" {
  name   = "nginx_elb_sg"
  vpc_id = aws_vpc.this_vpc.id

  #Allow HTTP from anyw re
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-elb-sg" }
  )
}

resource "aws_security_group" "this_nginx_server_sg" {
  name   = "nginx_server_sg"
  vpc_id = aws_vpc.this_vpc.id

  dynamic ingress {
    for_each = var.nginx_server_ports_ingress
    content {
      from_port   = ingress.key
      to_port     = ingress.key
      cidr_blocks = ingress.value
      protocol    = "tcp"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "net_vpc_id" {
  value = aws_vpc.this_vpc.id
}

output "net_vpc_subnets" {
  value = aws_subnet.this_subnet[*].id
}