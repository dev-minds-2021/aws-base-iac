##################################################################################
# VARIABLES:
##################################################################################

locals {
  #   bucket_name = "${var.bket_name_prefix}-${local.env_name}-${random_integer.this_random_vals.result}"
  env_name = lower(terraform.workspace)
  common_tags = {
    BillingCodes = var.billing_code_tag
    Environment  = local.env_name
  }
}

variable "region" {
  default = "eu-west-1"
}

# variable "env_name" {}
variable "billing_code_tag" {}

variable "net_addr_space" {
  type = map(string)
}

variable "subnet_count" {
  type = map(number)
}

variable "nginx_server_ports_ingress" {
  # type = list 
  default = {
    "22" = ["0.0.0.0/0"]
    "80" = ["0.0.0.0/0"]
  }
}

