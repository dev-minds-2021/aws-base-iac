billing_code_tag = "ACCT6589513"

net_addr_space = {
  Development = "10.0.0.0/16"
  UAT         = "10.1.0.0/16"
  Production  = "10.2.0.0/16"
  preprod     = "10.10.0.0/16"
}

subnet_count = {
  Development = 2
  UAT         = 2
  Production  = 3
  preprod     = 3
}
