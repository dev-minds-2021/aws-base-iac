#aws_profile      = "admin_master"
aws_profile      = "admin_preprod"
aws_region       = "eu-west-1"
billing_code_tag = "ACCT6589513"
bket_name_prefix = "dminds"
#private_key_path = "../../centrale-keys.pem"
private_key_path = "./preprod.pem"
key_name         = "preprod"
net_addr_space = {
  Development = "10.0.0.0/16"
  UAT         = "10.1.0.0/16"
  Production  = "10.2.0.0/16"
  prep-dev    = "10.2.0.0/16"
}

subnet_count = {
  Development = 2
  UAT         = 2
  Production  = 3
  prep-dev    = 1
}

instance_count = {
  Development = 2
  UAT         = 2
  Production  = 3
  prep-dev    = 3
}

instance_type = {
  Development = "t2.micro"
  UAT         = "t2.micro"
  Production  = "t2.medium"
  prep-dev    = "t2.micro"
}

