################################################
# AWS Provider Plgn: tf12.26 - provider ~> 3.16
################################################
provider "aws" {
  version = "~>2.0"
  region  = var.aws_region
  profile = var.aws_profile
}

data "aws_availability_zones" "this_ds_azs" {}
data "aws_ami" "this_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn-ami-hvm*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

###########################
# Generate Random strings 
###########################
resource "random_integer" "this_random_vals" {
  min = 1000000
  max = 9999900
}

resource "aws_vpc" "this_vpc" {
  cidr_block = var.net_addr_space[terraform.workspace]
  tags = merge(
    local.common_tags, { Name = "${local.env_name}-vpc" }
  )
}


resource "aws_internet_gateway" "this_igw" {
  vpc_id = aws_vpc.this_vpc.id

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-igw" }
  )
}

resource "aws_subnet" "this_subnet" {
  count                   = var.subnet_count[terraform.workspace]
  cidr_block              = cidrsubnet(var.net_addr_space[terraform.workspace], 8, count.index)
  vpc_id                  = aws_vpc.this_vpc.id
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.this_ds_azs.names[count.index]

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-subnet${count.index + 1}" }
  )
}

resource "aws_route_table" "this_rtb" {
  vpc_id = aws_vpc.this_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this_igw.id
  }

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-rtb" }
  )
}

resource "aws_route_table_association" "this_rtb_sb_assoc" {
  count          = var.subnet_count[terraform.workspace]
  subnet_id      = aws_subnet.this_subnet[count.index].id
  route_table_id = aws_route_table.this_rtb.id
}

resource "aws_security_group" "this_elb_sg" {
  name   = "nginx_elb_sg"
  vpc_id = aws_vpc.this_vpc.id

  #Allow HTTP from anyw re
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-elb-sg" }
  )
}

# resource "aws_security_group" "this_nginx_server_sg" {
#   name   = "nginx_server_sg"
#   vpc_id = aws_vpc.this_vpc.id

#   # SSH access from anywhere
#   ingress {
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   # HTTP access from the VPC
#   ingress {
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = [var.net_addr_space[terraform.workspace]]
#   }

#   # outbound internet access
#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   tags = merge(
#     local.common_tags, { Name = "${local.env_name}-nginx-server" }
#   )
# }

resource "aws_security_group" "this_nginx_server_sg" {
  name   = "nginx_server_sg"
  vpc_id = aws_vpc.this_vpc.id

  dynamic ingress {
    for_each = var.nginx_server_ports_ingress
    content {
      from_port   = ingress.key
      to_port     = ingress.key
      cidr_blocks = ingress.value
      protocol    = "tcp"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "this_elb" {
  name = "${local.env_name}-nginx-elb"

  subnets         = aws_subnet.this_subnet[*].id
  security_groups = [aws_security_group.this_elb_sg.id]
  instances       = aws_instance.this_nginx_servers[*].id

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-web-elb" }
  )
}


###########••••••••••############
resource "aws_instance" "this_nginx_servers" {
  count                  = var.instance_count[terraform.workspace]
  ami                    = data.aws_ami.this_ami.id
  instance_type          = var.instance_type[terraform.workspace]
  subnet_id              = aws_subnet.this_subnet[count.index % var.subnet_count[terraform.workspace]].id
  vpc_security_group_ids = [aws_security_group.this_nginx_server_sg.id]
  key_name               = var.key_name
  iam_instance_profile   = aws_iam_instance_profile.nginx_profile.name
  depends_on             = [aws_iam_role_policy.allow_s3_all]

  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ec2-user"
    private_key = file(var.private_key_path)
  }

  provisioner "file" {
    content     = <<EOF
    access_key = AKIAQK5OKAQJOKTYHECE
    secret_key = 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
    security_token =
    use_https = True
    bucket_location = US
    EOF
    destination = "/home/ec2-user/.s3cfg"
  }

  provisioner "file" {
    content     = <<EOF
    /var/log/nginx/*log {
    daily
    rotate 10
    missingok
    compress
    sharedscripts
    postrotate
    endscript
    lastaction
        INSTANCE_ID=`curl --silent http://169.254.169.254/latest/meta-data/instance-id`
        sudo /usr/local/bin/s3cmd sync --config=/home/ec2-user/.s3cfg /var/log/nginx/ s3://${aws_s3_bucket.web_bucket.id}/nginx/$INSTANCE_ID/
    endscript
  }
  EOF
    destination = "/home/ec2-user/nginx"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum install nginx -y",
      "sudo service nginx start",
      "sudo cp /home/ec2-user/.s3cfg /root/.s3cfg",
      "sudo cp /home/ec2-user/nginx /etc/logrotate.d/nginx",
      "sudo mkdir /home/ec2-user/build-point",
      "sudo chown ec2-user:ec2-user /home/ec2-user/build-point",
      "sudo rm -rf /usr/share/nginx/html/* ",
      "sudo pip install s3cmd",
      "s3cmd get s3://${aws_s3_bucket.web_bucket.id}/website/s3deploy.txt .",
      # "s3cmd get s3://${aws_s3_bucket.web_bucket.id}/website/Globo_logo_Vert.png .",
      "s3cmd get -r s3://${aws_s3_bucket.web_bucket.id}/website/ /home/ec2-user/build-point/",
      "sudo cp -rf /home/ec2-user/build-point/* /usr/share/nginx/html/",
      # "sudo cp /home/ec2-user/index.html /usr/share/nginx/html/index.html",
      # "sudo cp /home/ec2-user/Globo_logo_Vert.png /usr/share/nginx/html/Globo_logo_Vert.png",
      "sudo logrotate -f /etc/logrotate.conf"
    ]
  }

  tags = merge(
    local.common_tags, { Name = "${local.env_name}-nginx-server-${count.index + 1}" }
  )
}

resource "aws_iam_role" "allow_nginx_s3" {
  name = "${local.env_name}_allow_nginx_s3"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "nginx_profile" {
  name = "${local.env_name}_nginx_profile"
  role = aws_iam_role.allow_nginx_s3.name
}

resource "aws_iam_role_policy" "allow_s3_all" {
  name = "${local.env_name}_allow_s3_all"
  role = aws_iam_role.allow_nginx_s3.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": [
                "arn:aws:s3:::${local.bucket_name}",
                "arn:aws:s3:::${local.bucket_name}/*"
            ]
    }
  ]
}
EOF
}

resource "aws_s3_bucket" "web_bucket" {
  bucket        = local.bucket_name
  acl           = "private"
  force_destroy = true
  tags = merge(
    local.common_tags, { Name = "${local.env_name}-web-bucket" }
  )
}

resource "aws_s3_bucket_object" "website" {
  bucket = aws_s3_bucket.web_bucket.bucket
  key    = "/website/s3deploy.txt"
  source = "./s3deploy.txt"
}

# resource "aws_s3_bucket_object" "graphic" {
#   bucket = aws_s3_bucket.web_bucket.bucket
#   key    = "/website/Globo_logo_Vert.png"
#   source = "./Globo_logo_Vert.png"
# }

resource "null_resource" "remove_and_upload_to_s3" {
  provisioner "local-exec" {
    command = "aws s3 sync ${path.module}/app/ s3://${aws_s3_bucket.web_bucket.id}/website/"
  }
}

output "aws_elb_public_dns" {
  value = aws_elb.this_elb.dns_name
}

output "aaws_instance_pub_ip" {
  value = aws_instance.this_nginx_servers[*].public_ip
}
