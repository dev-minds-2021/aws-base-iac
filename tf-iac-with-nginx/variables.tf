variable "aws_profile" {}
variable "aws_region" {}
variable "private_key_path" {}
variable "bket_name_prefix" {}
variable "key_name" {}

variable "net_addr_space" {
  type = map(string)
}

variable "subnet_count" {
  type = map(number)
}

variable "instance_count" {
  type = map(number)
}

variable "instance_type" {
  type = map(string)
}


# variable "env_name" {}
variable "billing_code_tag" {}

variable "nginx_server_ports_ingress" {
  # type = list 
  default = {
    "22" = ["0.0.0.0/0"]
    "80" = ["0.0.0.0/0"]
  }
}


locals {
  bucket_name = "${var.bket_name_prefix}-${local.env_name}-${random_integer.this_random_vals.result}"
  env_name    = lower(terraform.workspace)
  common_tags = {
    BillingCodes = var.billing_code_tag
    Environment  = local.env_name
  }
}